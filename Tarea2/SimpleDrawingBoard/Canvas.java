package SimpleDrawingBoard;

import java.util.ArrayList;

public class Canvas {
    public ArrayList<String> canvasArray;

    public Canvas(int width, int height) {
        canvasArray = new ArrayList<>();
        width+=2;
        height+=2;
        for (int y=0;y<height;y++)
        {
            String canvas="";
            if(y==0 || y==height-1)
            {
                for(int x=0;x<width;x++)
                    canvas =canvas+"-";
                canvas = canvas+"\n";
            }else
            {
                for(int x=0;x<width;x++)
                {
                    if(x==0||x==width-1) canvas =canvas+"|";
                    else canvas=canvas+" ";
                }
                canvas = canvas+"\n";
            }
            canvasArray.add(canvas);
        }
    }

    public Canvas draw(int x1, int y1, int x2, int y2) {
        //En proceso
        return this;
    }

    public Canvas fill(int x, int y, char ch) {
        return this;
    }

    public String drawCanvas() {
        return "draw the canvas with borders";
    }

    public static void main(String[] args) {
        Canvas canvas = new Canvas(5,5);

        for(String string:canvas.canvasArray){
            System.out.print(string);
        }
    }
}
