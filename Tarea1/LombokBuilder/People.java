package LombokBuilder;

public class People {
    private int age;
    private String name;
    private String lastName;
    private String GREET="hello";

    public People(int age, String name, String lastName) {
        this.age = age;
        this.name = name;
        this.lastName = lastName;
    }

    public String greet(){
        return getGREET()+" my name is "+getName();
    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGREET() {
        return GREET;
    }

    public static void main(String[] args) {
        People pedro = new People(19,"Pedro","Lopez");

        System.out.println(pedro.greet());
    }
}

