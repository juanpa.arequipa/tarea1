package MisteryColor;

import java.util.ArrayList;
import java.util.List;

public class MysteryColorAnalyzerImpl implements MysteryColorAnalyzer{

    @Override
    public int numberOfDistinctColors(List<Color> mysteryColors) {
        List<Color> mysteryColorsFound = new ArrayList<>();
        for(Color color:mysteryColors){
            if(!mysteryColorsFound.contains(color)){
                mysteryColorsFound.add(color);
            }
        }
        return mysteryColorsFound.size();
    }

    @Override
    public int colorOccurrence(List<Color> mysteryColors, Color color) {
        int counter = 0;
        for (Color color_ :mysteryColors){
            if(color_.equals(color))
                counter++;
        }
        return counter;
    }

    public static void main(String[] args) {
        MysteryColorAnalyzerImpl mysteryColorAnalyzer = new MysteryColorAnalyzerImpl();

        List<Color> mysteryColors = new ArrayList<>();
        mysteryColors.add(Color.BLUE);
        mysteryColors.add(Color.RED);
        mysteryColors.add(Color.GREEN);
        mysteryColors.add(Color.BLUE);
        mysteryColors.add(Color.BLUE);
        mysteryColors.add(Color.BLUE);

        System.out.println(mysteryColorAnalyzer.numberOfDistinctColors(mysteryColors));
        System.out.println(mysteryColorAnalyzer.colorOccurrence(mysteryColors, Color.BLUE));
    }
}
