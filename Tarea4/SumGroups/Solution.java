package SumGroups;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    public static List<String> stringList;

    public static void convertArrayToStringList(int[] array) {
        stringList = new ArrayList<>();
        for (int element : array) {
            if (element % 2 == 0)
                stringList.add("par");
            else stringList.add("impar");
        }
    }

    public static boolean verifyIfListIsFinished(){
        for(int i=0;i<stringList.size()-1;i++){
            if(stringList.get(i).equals(stringList.get(i+1))) return false;
        }
        return true;
    }

    public static void simplifyEvenElements() {
        int i = 1;
        while (i < stringList.size()) {
            if (stringList.get(i).equals("par") && stringList.get(i - 1).equals("par")) {
                stringList.remove(i);
            } else i++;
        }
    }

    public static void simplifyOddElements() {
        int i = 1;
        int counter = 1;
        while (i < stringList.size()) {
            if (stringList.get(i).equals("impar") && stringList.get(i - 1).equals("impar")) {
                stringList.remove(i);
                counter+=1;
            }else {
                if(counter>1){
                    if(counter%2==0) stringList.set(i-1,"par");
                    counter=1;
                }
                i++;
            }
        }
        if(counter%2==0) stringList.set(i-1,"par");
    }

    public static int sumGroups(int[] arr) {
        convertArrayToStringList(arr);
        while (!verifyIfListIsFinished()) {
            simplifyEvenElements();
            simplifyOddElements();
        }
        return stringList.size();
    }
}